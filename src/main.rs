// Copyright 2020 William Salmon.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use druid::{
    lens::{self, LensExt},
    theme,
    widget::{Flex, Slider, Svg, SvgData, WidgetExt},
    AppLauncher, BoxConstraints, Color, Data, Env, Event, EventCtx, LayoutCtx, Lens, PaintCtx,
    Point, Rect, Size, UpdateCtx, Widget, WindowDesc,
};

use std::f64;

extern crate ndarray;
extern crate num;
extern crate time;

const PI: f64 = f64::consts::PI;

use structopt::StructOpt;

mod widgets;
use widgets::{FullData, Plotter};

mod graphing;
use graphing::{HasRect, MakePlot, Makeimgrid, XYIn};

/// Search for a pattern in a file and display the lines that contain it.
#[derive(StructOpt)]
struct Cli {
    #[structopt(short, long)]
    nogui: bool,
}

fn main() {
    let args = Cli::from_args();

    if args.nogui {
        let plotthing = Makeimgrid {};
        let tiger_svg = plotthing.makeplot(XYIn {
            slider_x: 0.5,
            slider_y: 0.5,
            ranges: HasRect {
                rect: Rect::ZERO.with_size(Size {
                    width: 1024.0,
                    height: 1024.0,
                }),
            },
        });
        return;
    };

    let main_window = WindowDesc::new(ui_builder);
    let data = FullData {
        sliders: XYIn {
            slider_x: 0.5,
            slider_y: 0.2,
            ranges: HasRect {
                rect: Rect::ZERO.with_size(Size {
                    width: 1024.0,
                    height: 1024.0,
                }),
            },
        },
    };
    AppLauncher::with_window(main_window)
        .configure_env(|env, _| {
            env.set(theme::SELECTION_COLOR, Color::rgb8(0xA6, 0xCC, 0xFF));
            env.set(theme::WINDOW_BACKGROUND_COLOR, Color::WHITE);
            env.set(theme::LABEL_COLOR, Color::BLACK);
            env.set(theme::CURSOR_COLOR, Color::BLACK);
            env.set(theme::BACKGROUND_LIGHT, Color::rgb8(230, 230, 230));
        })
        .use_simple_logger()
        .launch(data)
        .expect("launch failed");
}

fn ui_builder() -> impl Widget<FullData> {
    let plotthing = Makeimgrid {};
    let tiger_svg = plotthing.makeplot(XYIn {
        slider_x: 0.2,
        slider_y: 0.5,
        ranges: HasRect {
            rect: Rect::ZERO.with_size(Size {
                width: 1024.0,
                height: 1024.0,
            }),
        },
    });

    let mut col = Flex::column();
    let slider_a = Slider::new().lens(lens::Id.map(
        // Expose shared data with children data
        |d: &FullData| (d.sliders.slider_x),
        |d: &mut FullData, x: f64| {
            // If shared data was changed reflect the changes in our AppData
            d.sliders.slider_x = x
        },
    ));
    let slider_b = Slider::new().lens(lens::Id.map(
        // Expose shared data with children data
        |d: &FullData| (d.sliders.slider_y),
        |d: &mut FullData, x: f64| {
            // If shared data was changed reflect the changes in our AppData
            d.sliders.slider_y = x
        },
    ));

    //col.add_child(Svg::new(tiger_svg.clone()).fix_width(100.0).center(), 1.0);
    col.add_child(
        Plotter::new(tiger_svg, Box::new(plotthing)).lens(FullData::sliders),
        1.0,
    );
    col.add_child(slider_a, 0.0);
    col.add_child(slider_b, 0.0);
    col
}
