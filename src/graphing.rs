// Copyright 2020 William Salmon.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use druid::{widget::SvgData, Data, Lens, Rect, Size};

use plotlib::page::Page;
use plotlib::repr::Imgrid;

use std::f64;
use std::str::FromStr;

extern crate ndarray;
extern crate num;
extern crate time;

use num::Complex;

const PI: f64 = f64::consts::PI;

use rustfft::FFTplanner;

use std::fs::File;
use std::io::prelude::*;
//use svg::write_internal;

#[derive(Clone, Data, Lens, Debug, Copy)]
pub struct HasRect {
    #[druid(same_fn = "rect_same")]
    pub rect: Rect,
}

fn rect_same(one: &Rect, two: &Rect) -> bool {
    one.x0.same(&two.x0) && one.x1.same(&two.x1) && one.y0.same(&two.y0) && one.y1.same(&two.y1)
}

#[derive(Clone, Data, Lens, Debug, Copy)]
pub struct XYIn {
    pub slider_x: f64,
    pub slider_y: f64,
    pub ranges: HasRect,
}

impl XYIn {
    fn new() -> Self {
        XYIn {
            slider_x: 0.1,
            slider_y: 0.1,
            ranges: HasRect {
                rect: Rect::ZERO.with_size(Size {
                    width: 1024.0,
                    height: 1024.0,
                }),
            },
        }
    }
}

pub trait MakePlot {
    fn makeplot(&self, inputs: XYIn) -> SvgData;
}

#[derive(Clone, Copy)]
pub struct Makeimgrid {}

impl MakePlot for Makeimgrid {
    fn makeplot(&self, inputs: XYIn) -> SvgData {
        let plotarea: Rect = inputs.ranges.rect;
        let input_x = (inputs.slider_x - 0.5) * 2.0;
        let input_y = inputs.slider_y;
        let n = 1024;
        let freq = 2.0 * (10.0 as f64).powf(9.0); // 2GHz
        println!("make the data");
        let mut wave_number = vec![Complex::<f64>::new(0.0, 0.0); n * n];
        let mut phaser = vec![Complex::<f64>::new(0.0, 0.0); n * n];

        let mut output: Vec<Complex<f64>> = vec![Complex::<f64>::new(0.0, 0.0); n * n];

        let ThingX = (input_x * f64::consts::PI).cos() * input_y;
        let ThingY = (input_x * f64::consts::PI).sin() * input_y;

        for iii in 0..(n * n) {
            let y_iii = (iii / n) as i128;
            let x_iii = iii as i128 - y_iii * n as i128;
            let x_k = x_iii - (n / 2) as i128;
            let y_k = y_iii - (n / 2) as i128;
            wave_number[iii].im =
                2. * PI * ((y_k * 2) as f64 * ThingX + (x_k * 2) as f64 * ThingY) as f64;

            phaser[iii] = wave_number[iii].exp()
        }

        //println!("phaser {:?}", phaser);
        let mut planner = FFTplanner::new(false);
        let fft = planner.plan_fft(n);

        for ii in 0..(n) {
            fft.process(
                &mut phaser[ii * n..(ii + 1) * n],
                &mut output[ii * n..(ii + 1) * n],
            );
        }
        let mut transposed: Vec<Complex<f64>> = vec![Complex::<f64>::new(0.0, 0.0); n * n];
        for ii in 0..(n) {
            for inner in 0..n {
                transposed[ii * n + inner] = output[ii + inner * n];
            }
        }
        for ii in 0..(n) {
            fft.process(
                &mut transposed[ii * n..(ii + 1) * n],
                &mut output[ii * n..(ii + 1) * n],
            );
        }

        //let mut data1 = Vec::new();
        //let mut data2 = Vec::new();
        let mut data3 = Vec::new();

        let f_n = n as f64;
        let sf_n = f_n.powf(2.0);
        let n_i: i32 = n as i32;
        for iii in 0..(n * n) {
            let fi = iii as f64;
            //data1.push((fi, phaser[iii].norm()));
            //data2.push((fi, output[iii].norm() / sf_n));
            let yi: i32 = iii as i32 / n_i;
            let xi: i32 = iii as i32 - yi * n_i;
            let mut yy: i32 = 0;
            let mut xx: i32 = 0;
            if yi >= n_i / 2 {
                yy = yi - n_i / 2;
            } else {
                yy = yi + n_i / 2;
            };

            if xi >= n_i / 2 {
                xx = xi - n_i / 2;
            } else {
                xx = xi + n_i / 2;
            };
            let out = output[(xx + yy * n_i) as usize].norm();
            let out2 = out.log10() / sf_n.log10();
            data3.push(out2)
        }
        println!("ffted");
        let s1 = Imgrid::from_slice(&data3, n as u64, n as u64);
        println!("s1 made");

        // The 'view' describes what set of data is drawn
        let v = plotlib::view::ContinuousView::new()
            .add(s1)
            .x_range(plotarea.x0, plotarea.x1)
            .y_range(plotarea.y0, plotarea.y1)
            .x_label("Down Range")
            .y_label("Cross Range");
        // A page with a single view is then saved to an SVG file
        //Page::single(&v).save("scatter.svg");
        println!("v maded");
        let svg = Page::single(&v).to_svg().expect("Woah");
        println!("svg made");
        let mut vec_svg = Vec::new();
        svg::write(&mut vec_svg, &svg).expect("wrote it");
        let str_svg = String::from_utf8_lossy(&vec_svg);
        println!("svg saved");
        let mut file = File::create("foo.svg").expect("yes");
        file.write_all(&vec_svg).expect("no error");
        println!("filed");

        let tiger_svg: SvgData = match SvgData::from_str(&str_svg) {
            Err(why) => panic!("couldn't open {}", why),
            Ok(tiger_svg) => tiger_svg,
        };
        println!("svgdata made");

        tiger_svg
    }
}
