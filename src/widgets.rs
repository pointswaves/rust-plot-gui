// Copyright 2020 William Salmon.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::marker::PhantomData;

use druid::{
    widget::{Svg, SvgData},
    BoxConstraints, Data, Env, Event, EventCtx, LayoutCtx, Lens, LifeCycle, LifeCycleCtx, PaintCtx,
    Point, Rect, Size, UpdateCtx, Widget,
};

extern crate ndarray;
extern crate num;
extern crate time;

use crate::graphing::{HasRect, MakePlot, XYIn};

#[derive(Clone, Data, Lens, Debug)]
pub struct FullData {
    pub sliders: XYIn,
}

/// A widget that renders a SVG
pub struct Plotter<T> {
    svg_widget: Box<dyn Widget<u32>>,
    phantoma: PhantomData<T>,
    mouse_pos_cache: Point,
    svg_maker: Box<dyn MakePlot>,
}

impl Plotter<XYIn> {
    /// Create an SVG-drawing widget from SvgData.
    ///
    /// The SVG will scale to fit its box constraints.
    pub fn new(svg_data: SvgData, updatefunc: Box<dyn MakePlot>) -> Self {
        Plotter {
            svg_widget: Box::new(Svg::new(svg_data)),
            phantoma: Default::default(),
            mouse_pos_cache: Point { x: 0.0, y: 0.0 },
            svg_maker: updatefunc,
        }
    }
}

impl Widget<XYIn> for Plotter<XYIn> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut XYIn, _env: &Env) {
        match event {
            Event::MouseDown(mouse) => {
                println!("down {:?}", mouse);
            }
            Event::MouseMoved(mouse) => {
                println!("move {:?}", mouse);
                self.mouse_pos_cache = mouse.pos;
            }
            Event::MouseUp(mouse) => {
                println!("up {:?}", mouse);
            }
            Event::Wheel(mouse) => {
                println!("whell {:?}", mouse);

                let delt = mouse.delta.y * 0.1;
                data.ranges = HasRect {
                    rect: Rect::new(
                        data.ranges.rect.min_x() + delt,
                        data.ranges.rect.min_y() + delt,
                        data.ranges.rect.max_x() - delt,
                        data.ranges.rect.max_y() - delt,
                    ),
                };

                println!("rec {:?}", data.ranges);
                ctx.invalidate();
            }
            _ => (),
        }
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, _event: &LifeCycle, _data: &XYIn, _env: &Env) {
        ctx.invalidate();
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &XYIn, data: &XYIn, _env: &Env) {
        let newsvg = self.svg_maker.makeplot(*data);
        self.svg_widget = Box::new(Svg::new(newsvg));
        ctx.invalidate();
    }

    fn layout(
        &mut self,
        layout_ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _data: &XYIn,
        env: &Env,
    ) -> Size {
        self.svg_widget.layout(layout_ctx, bc, &0, env)
    }

    fn paint(&mut self, paint_ctx: &mut PaintCtx, _data: &XYIn, env: &Env) {
        self.svg_widget.paint(paint_ctx, &0, env);
        println!("repaint")
    }
}
